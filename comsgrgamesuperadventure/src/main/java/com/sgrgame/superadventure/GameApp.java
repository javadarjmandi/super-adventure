package com.sgrgame.superadventure;

import androidx.multidex.MultiDexApplication;
import ir.tapsell.sdk.Tapsell;
import me.cheshmak.android.sdk.core.Cheshmak;

public class GameApp extends MultiDexApplication {

    @Override
    public void onCreate () {
        super.onCreate();
        Tapsell.initialize(this, "arckfapqhhlcfteqlargqjgoibkmmacatlkmtdocggeflmsniciknipnletntflahcadmq");
        Cheshmak.with(this);
        Cheshmak.initTracker("ivc8WGovQB4/TZtwifr3Pg==");

    }
}