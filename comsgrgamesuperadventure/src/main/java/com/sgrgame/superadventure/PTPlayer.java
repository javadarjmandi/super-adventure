package com.sgrgame.superadventure;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;
import com.secrethq.store.PTStoreBridge;
import com.google.android.gms.games.GamesActivityResultCodes;

import com.secrethq.ads.*;
import com.secrethq.utils.*;
import com.sggame.superadventure.R;

import java.io.Serializable;

import androidx.appcompat.app.AlertDialog;
import ir.tapsell.sdk.Tapsell;
import ir.tapsell.sdk.TapsellAd;
import ir.tapsell.sdk.TapsellAdRequestListener;
import ir.tapsell.sdk.TapsellAdRequestOptions;
import ir.tapsell.sdk.TapsellAdShowListener;
import ir.tapsell.sdk.TapsellShowOptions;

public class PTPlayer extends Cocos2dxActivity implements onAdReadyListener, Serializable {

    private transient TapsellAd ad;
    private boolean hasViewedAd = false;

    private static native void loadModelController();

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.v("----------","onActivityResult: request: " + requestCode + " result: "+ resultCode);
            if(PTStoreBridge.iabHelper().handleActivityResult(requestCode, resultCode, data)){
                Log.v("-----------", "handled by IABHelper");
            }
            else if(requestCode == PTServicesBridge.RC_SIGN_IN){
                if(resultCode == RESULT_OK){
                    PTServicesBridge.instance().onActivityResult(requestCode, resultCode, data);
                }
                else if(resultCode == GamesActivityResultCodes.RESULT_SIGN_IN_FAILED){
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(this, "Google Play Services: Sign in error", duration);
                    toast.show();
                }
                else if(resultCode == GamesActivityResultCodes.RESULT_APP_MISCONFIGURED){
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(this, "Google Play Services: App misconfigured", duration);
                    toast.show();
                }
            }
        } catch (Exception e) {
            Log.v("-----------", "onActivityResult FAIL on iabHelper : " + e.toString());
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PTServicesBridge.initBridge(this, getString( R.string.app_id ));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        MobileAds.initialize(this, "ro-app-pub-3940256099942544~3347511713");


        loadAd(this);

    }

    private void loadAd (final onAdReadyListener ptPlayer) {
        final TapsellAdRequestOptions options = new TapsellAdRequestOptions(){
            @Override
            public int getCacheType () {
                return CACHE_TYPE_STREAMED;
            }

            @Override
            public void setCacheType (int i) {
                super.setCacheType(i);
            }

            @Override
            public boolean isContentEqualTo (TapsellAdRequestOptions tapsellAdRequestOptions) {
                return super.isContentEqualTo(tapsellAdRequestOptions);
            }
        };
        Tapsell.requestAd(this, "5c5d03ffdf59270001804324", options, new TapsellAdRequestListener() {
            private final String TAG = "TS:VIDEO";
            @Override
            public void onError (String s) {
                Log.d(TAG, s);
            }

            @Override
            public void onAdAvailable (TapsellAd tapsellAd) {
                ptPlayer.onReady(tapsellAd, PTPlayer.this);
            }

            @Override
            public void onNoAdAvailable () {

            }

            @Override
            public void onNoNetwork () {

            }

            @Override
            public void onExpiring (TapsellAd tapsellAd) {

            }
        });
    }

    @Override
    public void onNativeInit(){

//	    initBridges();
    }

//	private void initBridges(){
//		PTStoreBridge.initBridge( this );
//
//
//		if (PTJniHelper.isAdNetworkActive("kChartboost")) {
//			PTAdChartboostBridge.initBridge(this);
//		}
//
//		if (PTJniHelper.isAdNetworkActive("kRevMob")) {
//			PTAdRevMobBridge.initBridge(this);
//		}
//
//		if (PTJniHelper.isAdNetworkActive("kAdMob") || PTJniHelper.isAdNetworkActive("kFacebook")) {
//			PTAdAdMobBridge.initBridge(this);
//		}
//
//		if (PTJniHelper.isAdNetworkActive("kAppLovin")) {
//			PTAdAppLovinBridge.initBridge(this);
//		}
//
//		if (PTJniHelper.isAdNetworkActive("kLeadBolt")) {
//			PTAdLeadBoltBridge.initBridge(this);
//		}
//
//		if (PTJniHelper.isAdNetworkActive("kFacebook")) {
//			PTAdFacebookBridge.initBridge(this);
//		}
//
//		if (PTJniHelper.isAdNetworkActive("kHeyzap")) {
//			PTAdHeyzapBridge.initBridge(this);
//		}
//	}

    @Override
    public Cocos2dxGLSurfaceView onCreateView() {
        Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
        glSurfaceView.setEGLConfigChooser(8, 8, 8, 0, 0, 0);

        return glSurfaceView;
    }

    static {
        System.loadLibrary("player");
    }

//	@Override
//	protected void onResume() {
//		super.onResume();
//		if (PTJniHelper.isAdNetworkActive("kChartboost")) {
//			PTAdChartboostBridge.onResume( this );
//		}
//	}

//	@Override
//	protected void onStart() {
//		super.onStart();
//		if (PTJniHelper.isAdNetworkActive("kChartboost")) {
//			PTAdChartboostBridge.onStart( this );
//		}
//	}

//	@Override
//	protected void onStop() {
//		super.onStop();
//		if (PTJniHelper.isAdNetworkActive("kChartboost")) {
//			PTAdChartboostBridge.onStop( this );
//		}
//	}

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onReady (TapsellAd rAd, PTPlayer activity) {
        TapsellShowOptions showOptions = new TapsellShowOptions();
        showOptions.setBackDisabled(true);
        showOptions.setImmersiveMode(true);
        showOptions.setRotationMode(TapsellShowOptions.ROTATION_UNLOCKED);
        showOptions.setShowDialog(true);

        showOptions.setWarnBackPressedDialogMessage("آیا می‌خواهید خارج شوید؟");
        showOptions.setWarnBackPressedDialogMessageTextColor(Color.RED);
//                    showOptions.setWarnBackPressedDialogAssetTypefaceFileName("IranNastaliq.ttf");
        showOptions.setWarnBackPressedDialogPositiveButtonText("ادامه");
        showOptions.setWarnBackPressedDialogNegativeButtonText("خروج");
        showOptions.setWarnBackPressedDialogPositiveButtonBackgroundResId(R.drawable.button_background);
        showOptions.setWarnBackPressedDialogNegativeButtonBackgroundResId(R.drawable.button_background);
        showOptions.setWarnBackPressedDialogPositiveButtonTextColor(Color.RED);
        showOptions.setWarnBackPressedDialogNegativeButtonTextColor(Color.GREEN);
        showOptions.setWarnBackPressedDialogBackgroundResId(R.drawable.dialog_background);
        showOptions.setBackDisabledToastMessage("لطفا جهت بازگشت تا انتهای پخش ویدیو صبر کنید.");
        activity.ad = rAd;
        ad.show(activity, showOptions, new TapsellAdShowListener() {
            @Override
            public void onOpened (TapsellAd tapsellAd) {
                hasViewedAd = true;
            }

            @Override
            public void onClosed (TapsellAd tapsellAd) {
                hasViewedAd = true;
            }
        });
    }

    @Override
    public void onBackPressed () {
        if(hasViewedAd){
            super.onBackPressed();
        } else {
            AlertDialog alert = new AlertDialog.Builder(this)
                    .setMessage(R.string.view_ad)
                    .setNeutralButton("باشه", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick (DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create();
            alert.show();
            return;
        }
    }
}
